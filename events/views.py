from django.shortcuts import render, get_object_or_404
from dateutil.parser import parse
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from .models import Event, Attendance
from .forms import EventForm


def tonight(request):
	events = Event.objects.today().filter(latest=True)
	context = {
	'events': events,

	}
	return render(request, 'events/tonight.html', context)

@login_required
def create(request):
	form = EventForm(request.POST or None)
	if form.is_valid():
		event = form.save(commit=False)
		event.creator = request.user #Who is logged in?

		#Guess the date from the text
		guessed_date = None
		for word in event.description.split():
			try:
				guessed_date = parse(word)
				break
			except ValueError:
				continue

		event.start_date = guessed_date
		event.save()

		messages.success(request, "Your event was posted")

		if 'next' in request.POST:
			next = request.POST['next']
		else:
			next = reverse('events:ev_tonight')
		return HttpResponseRedirect(next)

	return render(request, 'events/create.html', {'form': form})

@login_required
def toggle_attendance(request):
	try:
		event_id = int(request.POST['event_id'])
	except (KeyError, ValueError):
		raise Http404

	event = get_object_or_404(Event, id=event_id)
	attendance, created = Attendance.objects.get_or_create(user=request.user,
		event=event)
	if created:
		messages.success(request, "Your are now attending {0}".format(event))
	else:
		attendance.delete()
		messages.success(request, "You are no longer attending {0}".format(event))

	next = request.POST.get('next', '')

	if not next:
		next = reverse('events:ev_tonight')

	return HttpResponseRedirect(next)



def archive(request):
	events = Event.objects.filter(latest=True)
	context = {
	'events': events
	}
	return render(request, 'events/tonight.html', context)
