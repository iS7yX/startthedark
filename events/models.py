from datetime import datetime, timedelta

from django.db import models
from django.db.models.query import QuerySet
from django.contrib.auth.models import User

def today():
	now = datetime.now()
	#start of the day
	start = datetime.min.replace(year= now.year, month=now.month, day=now.day)
	#end of the day
	end = (start + timedelta(days=1)) - timedelta.resolution
	return (start, end)


class EventQuerySet(QuerySet):
	
	def today(self):
		return self.filter(creation_date__range=today())


class EventManager(models.Manager):
	
	def get_queryset(self):
		return EventQuerySet(self.model)

	def today(self):
		return self.get_query_set().filter(creation_date__range=today())


class Event(models.Model):
	creation_date = models.DateTimeField(default=datetime.now)
	start_date = models.DateTimeField(null=True, blank=True)
	description = models.TextField()
	creator = models.ForeignKey(User, related_name='event_creator_set')
	attendees = models.ManyToManyField(User, through='Attendance')
	latest = models.BooleanField(default=True)

	objects = EventManager()

	def __unicode__(self):
		return self.description

	def save(self, **kwargs):
		Event.objects.filter(latest=True, creator=self.creator).today().update(latest=False)
		super(Event, self).save(**kwargs)


class Attendance(models.Model):
	user = models.ForeignKey(User)
	event = models.ForeignKey(Event)
	registration_date = models.DateTimeField(default=datetime.now)

	def __unicode__(self):
		return "{0} is attending {1}".format(self.user.username, self.event)

	class Meta:
		verbose_name_plural = 'attendance'


