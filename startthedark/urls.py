from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'startthedark.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^events/', 	include('events.urls', namespace='events')),
    url(r'^admin/', 	include(admin.site.urls)),
    url(r'^friends/', 	include('socialgraph.urls', namespace='sg')),

    )
