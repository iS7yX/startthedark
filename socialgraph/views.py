from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponseRedirect
from django.contrib.auth.models import User

from .models import UserLink
from .util import get_people_user_follows, get_people_following_user
from .util import get_mutual_followers

FRIEND_FUNCTION_MAP = {
	'followers': get_people_user_follows,
	'following': get_people_following_user,
	'mutual': get_mutual_followers,
}

def friend_list(request, list_type, username):
	user = get_object_or_404(User, username=username)
	context = {
	'list_type': list_type,
	'friends': FRIEND_FUNCTION_MAP[list_type](user),
	}

	return render(request, 'socialgraph/friend_list.html', context)
